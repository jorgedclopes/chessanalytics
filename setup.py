from setuptools import setup

setup(
    name='ChessAnalytics',
    version='0.0.1',
    packages='',
    url='',
    license='',
    maintainer='Jorge Lopes',
    maintainer_email='jorgedclopes@gmail.com',
    description='Insights about one\'s chess games.',
    include_package_data=True
)
